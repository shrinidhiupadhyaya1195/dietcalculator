#ifndef APPMANAGER_H
#define APPMANAGER_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QStandardPaths>
#include <QFile>
#include <QJsonArray>

class AppManager : public QObject
{
    Q_OBJECT
public:
    explicit AppManager(QObject *parent = nullptr);

    Q_PROPERTY(QJsonArray recipeData READ recipeData WRITE setRecipeData NOTIFY recipeDataChanged);
    Q_PROPERTY(QString configError READ configError WRITE setConfigError NOTIFY configErrorChanged);

    QJsonArray recipeData() const { return m_recipeData; }
    QString configError() const { return m_configError; }

    void setRecipeData(QJsonArray);
    void setConfigError(QString);

    Q_INVOKABLE void searchRecipe(QString);

private:
    QNetworkAccessManager* m_netAccessMgr;
    QString m_endPoint;
    QString m_configError;
    QJsonArray m_recipeData;

    void sortData(QByteArray);

signals:
    void recipeDataChanged(QJsonArray);
    void configErrorChanged(QString);
};

#endif // APPMANAGER_H
