import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

import DietCalculator 1.0

import "qrc:/screenManager"
import "qrc:/components"

ApplicationWindow {
    id: windowRoot

    visibility: Qt.platform.os === "android" ? Window.FullScreen : Window.Maximized
    visible: true
    title: qsTr("Diet Calculator")

    QtObject {
        id: commonProperties
        /**
          Check Whether Orientation Is Portrait or Landscape
          */
        property bool portrait: windowRoot.height > windowRoot.width

        /**
          Total Calorie Counter
          */
        property real totalCalorieCount: 0
    }

    /**
      Function For Calculate Whether To Add or Subtract Total Calories
      */
    function setTotalCalories(add,value) {
        console.log("SetTotalCalories",add,value)
        switch(add) {
        case true:
            commonProperties.totalCalorieCount = commonProperties.totalCalorieCount + value;
            break;
        case false:
            commonProperties.totalCalorieCount = commonProperties.totalCalorieCount - value;
            break;
        default:
            console.log("Not A Valid Value,Valid Values Add:true, Subtract:false");
            break;
        }
    }

    /**
      Function To Subtract Total Calories and Delete The Item From My Reciepe Model
      */
    function removeMyRecipe(index,value) {
        if(index > -1) {
            console.log("Remove My Receipe",index,value)
            setTotalCalories(false,value)
            myRecipeModel.remove(index)
        } else {
            console.log("Wrong Index")
        }
    }

    ListModel {
        id: myRecipeModel
    }

    SwipeView {
        id: swipeView

        width: parent.width
        height: windowRoot.height - footer.height

        currentIndex: footerTabBar.currentIndex

        DashBoard { id: dashboardScreen }

        AddRecipe { id: addRecipeScreen }
    }

    footer:TabBar {
        id: footerTabBar

        width: windowRoot.width
        height: commonProperties.portrait ? windowRoot.height * 0.05 : windowRoot.height * 0.1

        currentIndex: swipeView.currentIndex
        padding: 0

        Repeater {
            model: ThemeDefinitions.footerTabBarData

            TabButton {
                id: tabButton

                text: modelData

                background: Rectangle {
                    id: tabBackgroundRect

                    color: ThemeDefinitions.backGroundColor
                }

                contentItem: DText {
                    id: tabButtonText

                    anchors.centerIn: parent

                    text: tabButton.text
                    color: footerTabBar.currentIndex === index ? ThemeDefinitions.majorColorCode : ThemeDefinitions.disabledColor
                    scale: footerTabBar.currentIndex === index ? ThemeDefinitions.pressedScaleFactor : ThemeDefinitions.scaleFactor
                    font.pixelSize: footerTabBar.height / 1.5
                }
            }
        }
    }
}
