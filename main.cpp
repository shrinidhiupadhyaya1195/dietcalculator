#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "Appmanager.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    AppManager *appManager = new AppManager();

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    qmlRegisterSingletonType(QUrl("qrc:/components/ThemeDefinitions.qml"),"DietCalculator",1,0,"ThemeDefinitions");
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.rootContext()->setContextProperty("appManager",appManager);
    engine.load(url);


    return app.exec();
}
