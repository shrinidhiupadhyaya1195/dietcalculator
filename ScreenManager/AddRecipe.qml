import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import DietCalculator 1.0

import "qrc:/components"

/**
  Screen For Adding Recipe
  */

Item {
    id: root

    implicitWidth: parent.width
    implicitHeight: parent.height

    QtObject {
        id: _internal
        /**
      Factor For Width,Height,FontSize
      */
        readonly property double factor: 0.1

        /**
      To Saved Searched Item
      */
        property string searchedRecipe;

        /**
      Function To Search Recipe
      */
        function searchRecipe(value) {
            console.log("Search Recipe",value,_internal.searchedRecipe)
            if(_internal.searchedRecipe !== value) {
                appManager.searchRecipe(value)
            }
        }
    }

    ColumnLayout {
        id: mainLayout

        anchors.fill: parent

        spacing: 10

        Item {
            id: searchItem

            Layout.fillWidth: true
            Layout.fillHeight: false
            Layout.preferredHeight: parent.height * _internal.factor

            RowLayout {
                id: searchRowLayout

                width: parent.width
                height: parent.height

                spacing: 0

                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    TextField {
                        id: recipeText

                        width: parent.width
                        height: parent.height

                        placeholderText: "Search"
                        font.pixelSize: parent.height / 2

                        onAccepted: {
                            _internal.searchRecipe(text)
                        }
                    }
                }

                DButton {
                    id: searchButton

                    Layout.fillWidth: false
                    Layout.preferredWidth: parent.width * 0.3
                    Layout.fillHeight: true

                    color: ThemeDefinitions.majorColorCode
                    text: "SEARCH"
                    enabled: recipeText.displayText.length > 0
                    opacity: recipeText.displayText.length > 0 ? ThemeDefinitions.enabledOpacity : ThemeDefinitions.disabledOpacity
                    fontSize: height / 2.5

                    onButtonClicked: {
                        _internal.searchRecipe(recipeText.text)
                    }
                }
            }
        }

        DText {
            id: errorText

            Layout.fillWidth: true
            Layout.fillHeight: false
            Layout.preferredHeight: parent.height * _internal.factor / 2

            text: "ERROR:" + appManager.configError
            color: "red"
            visible: !!appManager.configError
        }

        Item {
            id: searchResultsItem

            Layout.fillWidth: true
            Layout.fillHeight: true

            DListView {
                id: searchResultsListView

                anchors.fill: parent

                model: appManager.recipeData

                onModelChanged: {
                    if(model.length > 0) {
                        _internal.searchedRecipe = recipeText.text
                    }
                }

                delegate: DBox {
                    id: addBox

                    width: searchResultsListView.width - (searchResultsListView.width * 0.11)
                    height: searchResultsListView.height * 0.2

                    leftText: appManager.recipeData[index].label
                    rightText: Math.round(appManager.recipeData[index].calories)
                    buttonText: ThemeDefinitions.addText
                    buttonColor: ThemeDefinitions.majorColorCode
                    fontSize: height / 3

                    onButtonClicked: {
                        myRecipeModel.append({"label": leftText,"calorie": parseInt(rightText),
                                                 "consumedTime": Qt.formatDateTime(new Date(),ThemeDefinitions.dateTimeFormat)});
                    }
                }
            }
        }

        Item {
            id: myRecipeItem

            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                id: consumedMealsColumnLayout

                anchors.fill: parent

                DText {
                    id: myItemLabelText

                    Layout.fillWidth: true
                    Layout.fillHeight: false
                    Layout.preferredHeight: parent.height * _internal.factor
                    Layout.alignment: Qt.AlignHCenter

                    text: "Consumed Meals"
                    color: ThemeDefinitions.majorColorCode
                    font.pixelSize:  parent.height * _internal.factor
                }

                DListView {
                    id: myRecipeListView

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    model: myRecipeModel

                    delegate: DBox {
                        id: deleteBox

                        width: myRecipeListView.width - (myRecipeListView.width * 0.11)
                        height: myRecipeListView.height * 0.2

                        leftText: label
                        rightText: calorie
                        buttonText: ThemeDefinitions.deleteText
                        buttonColor: ThemeDefinitions.deleteColorCode
                        fontSize: height / 3

                        onButtonClicked: {
                            windowRoot.removeMyRecipe(index,calorie)
                        }

                        Component.onCompleted: {
                            windowRoot.setTotalCalories(true,calorie)
                        }
                    }
                }
            }
        }
    }
}

