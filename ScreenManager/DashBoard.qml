import QtQuick 2.12
import QtQuick.Layouts 1.12

import DietCalculator 1.0

import "qrc:/components"

/**
  Dashboard Screen With Total Calories Consumed
  */

Item {
    id: root

    /**
      Factor For Height,Width
      */
    readonly property double factor: 0.1

    implicitWidth: parent.width
    implicitHeight: parent.height

    ColumnLayout {
        id: mainLayout

        anchors.fill: parent

        RowLayout {
            id: totalCaloriesLayout

            Layout.fillWidth: true
            Layout.fillHeight: false
            Layout.preferredHeight: parent.height * root.factor

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true

                DText {
                    id: caloriesLabelText

                    anchors.centerIn: parent

                    text: "Calories"
                    color: ThemeDefinitions.majorColorCode
                    font.pixelSize: parent.height / 1.6
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true

                DText {
                    id: recipeText

                    anchors.centerIn: parent

                    text: commonProperties.totalCalorieCount
                    color: ThemeDefinitions.majorColorCode
                    font.pixelSize: parent.height / 2
                }
            }
        }

        DListView {
            id: mealsConsumedListView

            Layout.fillWidth: true
            Layout.fillHeight: true

            model: myRecipeModel

            delegate: DBox {
                id: deleteBox

                width: mealsConsumedListView.width - (mealsConsumedListView.width * 0.11)
                height: mealsConsumedListView.height * 0.2

                leftText: label
                rightText: calorie
                buttonText: ThemeDefinitions.deleteText
                buttonColor: ThemeDefinitions.deleteColorCode
                fontSize: deleteBox.height / 4

                DText {
                    id: timeDateText

                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: deleteBox.bottom

                    text: consumedTime
                    font.pixelSize: parent.height * 0.15
                }

                onButtonClicked: {
                    windowRoot.removeMyRecipe(index,calorie)
                }
            }
        }
    }
}
