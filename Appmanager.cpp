#include "Appmanager.h"
#include "config.h"

AppManager::AppManager(QObject *parent)
    : QObject(parent),
      m_netAccessMgr(new QNetworkAccessManager(this))
{
}

void AppManager::searchRecipe(QString recipe) {
    qDebug() << "Search Recipe" << recipe;

    m_endPoint = QString("https://api.edamam.com/search?q=%1&app_id=%2&app_key=%3").arg(recipe, APP_ID, APP_KEY);
    QNetworkRequest request(m_endPoint);
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);
    auto networkReply = m_netAccessMgr->get(request);

    connect(networkReply, &QNetworkReply::finished, [=] {
        if (!networkReply->error()) {
            qDebug() << "Successfull Response";
            auto data = networkReply->readAll();
            sortData(data);
            setConfigError("");
        } else {
            setConfigError(networkReply->errorString());
        }
    });

    connect(networkReply,
            static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
            [=](QNetworkReply::NetworkError code) {
        qDebug() << "Error connecting to backend:" << networkReply->errorString() << endl
                 << "Error code:" << code;
        qDebug() << networkReply->readAll();
        setConfigError(networkReply->errorString());
    });
}

void AppManager::sortData(QByteArray data) {
    qDebug() << "Sort Data";

    QJsonParseError err;
    QJsonObject configData = QJsonDocument::fromJson(data, &err).object();
    QJsonArray hitsArray = configData["hits"].toArray();
    QJsonArray recipeSearchArray;

    for(int i=0;i < hitsArray.size();i++) {
        QJsonObject recipeObj = hitsArray[i].toObject()["recipe"].toObject();
        QJsonObject tempRecipeObj;

        tempRecipeObj.insert("calories",recipeObj["calories"]);
        tempRecipeObj.insert("label",recipeObj["label"]);
        recipeSearchArray.append(tempRecipeObj);
    }

    qDebug() << recipeSearchArray;

    setRecipeData(recipeSearchArray);
}

void AppManager::setRecipeData(QJsonArray recipeData) {
    qDebug() << "Set Recipe Data" << m_recipeData << recipeData;
    if (m_recipeData == recipeData) return;

    m_recipeData = recipeData;

    emit recipeDataChanged(m_recipeData);
}

void AppManager::setConfigError(QString configError) {
    qDebug() << "Set Config Error" << m_configError << configError;

    if (m_configError == configError) return;

    m_configError = configError;

    emit configErrorChanged(configError);
}

