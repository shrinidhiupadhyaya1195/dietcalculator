import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import DietCalculator 1.0

/**
  Custom ListView
  */

Item {
    id: root

    /**
      Model of The ListView
      */
    property alias model: listView.model
    /**
      Delegate of The ListView
      */
    property alias delegate: listView.delegate

    Rectangle {
        width: parent.width - (parent.width * 0.1)
        height: parent.height
        anchors.centerIn: parent

        color: ThemeDefinitions.transparentColorCode
        border.color: ThemeDefinitions.disabledColor

        ListView {
            id: listView

            anchors.fill: parent

            clip: true

            ScrollBar.vertical: ScrollBar {
                policy: ScrollBar.AsNeeded
            }
        }
    }
}
