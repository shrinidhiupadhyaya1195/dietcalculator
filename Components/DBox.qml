import QtQuick 2.12
import QtQuick.Layouts 1.12

import DietCalculator 1.0

/**
  Box with 2 Texts and Button Component
  */

Rectangle {
    id: root

    /**
      Left Side Text
      */
    property alias leftText: leftText.text
    /**
      Right Side Text
      */
    property alias rightText: rightText.text
    /**
      Text Of The Button on Right
      */
    property alias buttonText: button.text
    /**
      Button Color
      */
    property alias buttonColor: button.color
    /**
      Font Size For Text
      */
    property alias fontSize: leftText.font.pixelSize

    /**
      Factor For Height,Width
      */
     property double factor: 0.25

    /**
      Signal Emitted When Button is Clicked
      */
    signal buttonClicked();

    color: ThemeDefinitions.backGroundColor
    border.color: ThemeDefinitions.disabledColor

    RowLayout {
        id: mainLayout

        anchors.fill: parent

        spacing: 0

        DText {
            id: leftText

            Layout.fillWidth: true
            Layout.fillHeight: true

            font.pixelSize: root.height
        }

        DText {
            id: rightText


            Layout.fillWidth: false
            Layout.preferredWidth: mainLayout.width * root.factor
            Layout.fillHeight: true

            font.pixelSize: leftText.font.pixelSize
        }

        Item {
            Layout.fillWidth: false
            Layout.preferredWidth: parent.width * root.factor
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

            DButton {
                id: button

                width: parent.width
                height: parent.height - (parent.height * root.factor)
                anchors.verticalCenter: parent.verticalCenter

                color: ThemeDefinitions.majorColorCode

                onButtonClicked: {
                    root.buttonClicked()
                }
            }
        }
    }
}
