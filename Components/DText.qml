import QtQuick 2.12

/**
  Custom Text
  */

Text {
    id: root

    font.bold: true
    elide: Text.ElideRight
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter

    Behavior on scale {
        NumberAnimation { duration: 150 }
    }
}
