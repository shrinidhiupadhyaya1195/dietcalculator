import QtQuick 2.12

/**
  Custom Button
  */

Rectangle {
    id: root

    /**
      Button Text
      */
    property alias text: buttonText.text
    /**
      Text FontSize
      */
    property alias fontSize: buttonText.font.pixelSize

    /**
      Signal Emitted When Button is Clicked
      */
    signal buttonClicked();

    MouseArea {
        id: rootMouseArea

        anchors.fill: parent

        onClicked: {
            root.forceActiveFocus()
            root.buttonClicked()
        }
    }

    DText {
        id: buttonText

        anchors.centerIn: parent

        font.pixelSize: root.height
    }
}

