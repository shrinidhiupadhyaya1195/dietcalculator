pragma Singleton

import QtQuick 2.12

QtObject {
    /**
      Color Code For Buttons, TabBar, Text
      */
    readonly property string transparentColorCode: "transparent"
    /**
      Color Code For Buttons, TabBar, Text
      */
    readonly property string majorColorCode: "#21be2b"
    /**
      Color Code For BackGround of Buttons, Rectangles
      */
    readonly property string backGroundColor: "#FFFFFF"
    /**
      Color Code For Delete Buttons
      */
    readonly property string deleteColorCode: "#ff4000"
    /**
      Disabled Color Code For Buttons,Rectangles
      */
    readonly property string disabledColor: "#808080"
    /**
      Number Of Tab Bars And Data(Text) For It
      */
    readonly property var footerTabBarData:  ["Dashboard", "Add"]


    /**
      Value For Disabled Element Opacity
      */
    property double disabledOpacity: 0.4
    /**
      Value For Enabled Element Opacity
      */
    property double enabledOpacity: 1
    /**
      Value For Scale When Element Pressed
      */
    property double pressedScaleFactor: 1.2
    /**
      Normal Scale Value
      */
    property double scaleFactor: 1

    /**
      Date Format For Saving Date and Time
      */
    property string dateTimeFormat: "dd/MM/yyyy hh:mm"
    /**
      Text For Add
      */
    property string addText: "+"
    /**
      Text For Delete
      */
    property string deleteText: "-"
}

