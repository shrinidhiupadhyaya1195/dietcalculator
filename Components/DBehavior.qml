import QtQuick 2.12

Item {

    property string targetProperty;
//    property real duration: 5000 //150

    Behavior on targetProperty {
        NumberAnimation { duration: 150 }
    }
}
